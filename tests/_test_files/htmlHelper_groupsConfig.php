<?php

declare(strict_types=1);

return [
    'css' => [
        '//_test_files/css/paths_prepend.css',
        '//_test_files/css/styles.css',
    ],

    'less' => [
        '//_test_files/main.less',
    ],

    'scss' => [
        '//_test_files/main.scss',
    ],
];
